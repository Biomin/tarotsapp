FROM ubuntu:20.04

USER root

ENV DEBIAN_FRONTEND=noninteractive
RUN ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN apt update && apt -y upgrade
RUN apt install -y virtualbox virtualbox-dkms linux-headers-generic
RUN mkdir -p /vbox/vms \\ 
    mkdir -p /vbox/vdisks \\
    mkdir -p /vbox/iso

ARG SOURCE_WIN_ISO
COPY ${SOURCE_WIN_ISO} /vbox/iso/winxp.iso

RUN VBoxManage createvm --name winxp32 --register --basefolder /vbox/vms/
RUN VBoxManage createhd --filename /vbox/vdisks/winxp32.vdi --size 10240 --variant Standard

RUN VBoxManage storagectl winxp32 --name "IDE Controller" --add ide --controller PIIX4 --bootable on
RUN VBoxManage storageattach winxp32 --storagectl "IDE Controller" --type dvddrive --port 1 --device 0 --medium emptydrive
RUN VBoxManage storageattach winxp32 --storagectl "IDE Controller" --type hdd --port 0 --device 0 --medium /vbox/vdisks/winxp32.vdi
RUN VBoxManage modifyvm winxp32 --dvd /vbox/iso/winxp.iso
RUN VBoxManage modifyvm winxp32 --boot1 dvd
RUN VBoxManage modifyvm winxp32 --boot2 disk
RUN VBoxManage modifyvm winxp32 --memory 1024
RUN VBoxManage modifyvm winxp32 --nic1 nat
RUN VBoxManage modifyvm winxp32 --cableconnected1 on
RUN VBoxManage modifyvm winxp32 --nicpromisc1 allow-all
RUN VBoxManage modifyvm winxp32 --natpf1 "smb139,tcp,,139,10.0.2.15,139"

RUN VBoxManage modifyvm winxp32 --natpf1 "smb445,tcp,,445,10.0.2.15,445"


ENTRYPOINT [ 'VBoxHeadless', '-v', 'on', '-startvm', 'winxp32']
